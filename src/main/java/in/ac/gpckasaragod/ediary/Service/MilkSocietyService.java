/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.ediary.Service;

import in.ac.gpckasaragod.ediary.model.ui.data.MilkSociety;


import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface MilkSocietyService {
   public String updateMilkSociety(Integer id,String name,Date date); 
   public String saveMilksocietyDetails(String name,Date date);
   public MilkSociety readMilkSociety(Integer Id);
   public List<MilkSociety> getAllMilkSociety();
   public String deleteMilkSociety(Integer id);
}
