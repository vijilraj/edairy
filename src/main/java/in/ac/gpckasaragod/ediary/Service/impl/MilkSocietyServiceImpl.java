package in.ac.gpckasaragod.ediary.Service.impl;

import in.ac.gpckasaragod.ediary.Service.MilkSocietyService;
import in.ac.gpckasaragod.ediary.model.ui.data.MilkSociety;
import in.ac.gpckasaragod.ediary.ui.MilkSocietyDetailsForm;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

    
/**
 
 * @author student
 */
public class MilkSocietyServiceImpl extends ConnectionServiceImpl implements MilkSocietyService{
    

    @Override
    public MilkSociety readMilkSociety(Integer Id){
        MilkSociety milkSociety = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM MILK_SOCIETY WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Date date= resultSet.getDate("DATE");
                milkSociety= new MilkSociety(id,name,date);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(MilkSocietyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return milkSociety;
       
    }
    @Override
     public List<MilkSociety> getAllMilkSociety(){
        List<MilkSociety> milksociety= new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT MILK_SOCIETY.ID,NAME,DATE,VERIFY FROM EDIARY JOIN MILK SOCIETY.ID=SOCIETY_ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Date date= resultSet.getDate("DATE");
                MilkSociety milkSociety = new MilkSociety(id,name,date);
                milksociety.add(milkSociety);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(MilkSocietyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        List<MilkSociety> milkSociety = null;
        return milksociety;
    
     }

    @Override
     public String updateMilkSociety(Integer id,String name,Date date) {
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            String stringDate=sdf.format(date);
            
        String query = "UPDATE MILK_SOCIETY SET NAME='"+name+"',DATE='"+stringDate+"', WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
     }  
    @Override
     public String deleteMilkSociety(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM MILK_SOCIETY WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
     
}

    @Override
    public String saveMilksocietyDetails(String name, Date date) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String pattern = "yyyy-MM-dd";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

String formatedate = simpleDateFormat.format(date);
System.out.println(date);

            String query = "INSERT INTO MILK_SOCIETY(NAME,DATE) Values ('"+name+"','"+formatedate+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "saved succesfully";
            }
           
    
           } catch (SQLException ex) {
            Logger.getLogger(MilkSocietyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
          return "Save failed";
    }

   

}


